package com.company;

public class Penguin extends Birds{
    private final String name = "Penguin";
    private final String  area = "Antarctica";
    private final String food = "small fish, krill";

    @Override
    public String getEat() {
        return name + " can eat " + food;
    }

    @Override
    public String getFly() {
        return name + " can\'t fly";
    }

    @Override
    public String getRun() {
        return name + " can run";
    }

    @Override
    public String getSwim() {
        return name + " can swim in " + area;
    }

    @Override
    public String getScream() {
        return name + " can scream squawk/high-pitch braying";
    }
}
