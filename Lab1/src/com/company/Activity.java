package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Activity {
    
    private final static Scanner scanner = new Scanner(System.in);

    public void printMenu() {
        System.out.println("Enter the option: ");
        System.out.println("1 - Duck");
        System.out.println("2 - Swallow");
        System.out.println("3 - Ostrich");
        System.out.println("4 - Eagle");
        System.out.println("5 - Penguin");
        System.out.println("6 - Kiwi");
        System.out.println("0 - Exit");
    }

    public String readCommand() {
        return scanner.nextLine();
    }

    public void initActivity() {
        while (true) {
            printMenu();
            switch(readCommand()){
                case "0":
                    System.exit(0);
                    break;
                case "1":
                    Birds duck = new Duck();
                    System.out.println(duck.getEat()+". "+ duck.getFly()+". "+duck.getRun()+". \n"+duck.getSwim()+". "+duck.getScream()+". ");
                    break;
                case "2":
                    Birds swallow = new Swallow();
                    System.out.println(swallow.getEat()+". \n"+swallow.getFly()+". \n"+swallow.getRun()+". "+swallow.getSwim()+". "+swallow.getScream()+". ");
                    break;
                case "3":
                    Birds ostrich = new Ostrich();
                    System.out.println(ostrich.getEat()+". "+ostrich.getFly()+". "+ostrich.getRun()+". \n"+ostrich.getSwim()+". "+ostrich.getScream()+". ");
                    break;
                case "4":
                    Birds eagle = new Eagle();
                    System.out.println(eagle.getEat()+". "+eagle.getFly()+". "+eagle.getRun()+". \n"+eagle.getSwim()+". "+eagle.getScream()+". ");
                    break;
                case "5":
                    Birds penguin = new Penguin();
                    System.out.println(penguin.getEat()+". "+penguin.getFly()+". "+penguin.getRun()+". \n"+penguin.getSwim()+". "+penguin.getScream()+". ");
                    break;
                case "6":
                    Birds kiwi = new Kiwi();
                    System.out.println(kiwi.getEat()+". "+kiwi.getFly()+". "+kiwi.getRun()+". \n"+kiwi.getSwim()+". "+kiwi.getScream()+". ");
                    break;
                default:
                    System.out.println("Incorrect comand!");
            }
        }
    }
}
