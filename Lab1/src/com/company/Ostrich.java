package com.company;

public class Ostrich extends Birds{
    private final static String name = "Ostrich";
    private final static String area = "Africa";
    private final static String food = "vegetation(roots, flowers, fruits)";

    @Override
    public String getEat() {
        return name + " can eat " + food;
    }

    @Override
    public String getFly() {
        return name + " can\'t fly";
    }

    @Override
    public String getRun() {
        return name + " can run in "+ area ;
    }

    @Override
    public String getSwim() {
        return name + " can\'t swim ";
    }

    @Override
    public String getScream() {
        return name + " can scream 🔊 guu guuu gu and hissing noise";
    }
}
