package com.company;

public class Duck extends Birds {
    private final static String name = "Duck";
    private final static String area = "Lake Stars";
    private final static String food = "corn";

    @Override
    public String getEat() {
        return name + " can eat " + food;
    }

    @Override
    public String getFly() {
        return name + " can fly over " + area;
    }

    @Override
    public String getRun() {
        return name + " can run near " + area;
    }

    @Override
    public String getSwim() {
        return name + " can swim in " + area;
    }

    @Override
    public String getScream() {
        return name + " can scream ckria ckria";
    }
}
