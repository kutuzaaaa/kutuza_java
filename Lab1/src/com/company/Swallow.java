package com.company;

public class Swallow extends Birds{
    private final static String name = "Swallow";
    private final static String area = "cities in warm area";
    private final static String food = "worms, flies";

    @Override
    public String getEat() {
        return name + " can eat " + food;
    }

    @Override
    public String getFly() {
        return name + " can fly in " + area + " and catch " + food;
    }

    @Override
    public String getRun() {
        return name + " can\'t run in " + area;
    }

    @Override
    public String getSwim() {
        return name + " can\'t swim ";
    }

    @Override
    public String getScream() {
        return name + " can scream a chirp + a whine + a gurgle";
    }
}
