package com.company;

public class Birds {

    public String getFly(){
        return "The bird can/can\'t fly";
    }
    public String getRun(){
        return "The bird can/can\'t run";
    }
    public String getSwim(){
        return "The bird can/can\'t swim";
    }
    public String getEat(){
        return "The bird can eat";
    }
    public String getScream(){
        return "The bird can scream";
    }

}
